%% This code estimate laterality indices across the parameter space of global coupling 
% (k) and time delay (t). It simulates Kuramoto oscillators driven by external 
% 40 Hz sin wave input (See Figure3 in Kumar et al. 2020)

% Written by Neeraj Kumar, Nov 2020.

% External files needed
% # Kuramoto_simulation      -- function                    
% # cortical_network          -- mat file 
% From Abeysuriya et al  ('doi: 10.1371/journal.pcbi.1006007')
% ('https://github.com/OHBA-analysis/abeysuriya_wc_isp/tree/master/data_files')

clear;clc;restoredefaultpath

% Please specify paths of folder where the data is downloaded
% supp_path = '/....'; 
% save_path = '/.....';

load([supp_path 'cortical_network.mat'], 'conn','dist','left');


weights = conn;
tract_lengths = dist*1000; % in mm
N=size(weights,1);

% Initialize parameters 
t_max=25;
dt=0.0001;
sampling=100; sig_n=0.; 
sig_f=0; frequency_mean =10;
% f_dist =normrnd(frequency_mean,sig_f,[N 1]);
f_dist=zeros(N,1);

k_vals =1:50; % k simulation range
tau_vals = 4:16;% tau simulation range
LI_Ps = zeros(max(k_vals),max(tau_vals));
h = waitbar(0,'Please wait...');


%  ***************************** Binaural ********************************
AC.nodes = [29 63];%[Left auditory node  Right auditory node]
%     Scaling power of nodes in case of Binaural condition both would be 1
AC.scale_left  = 1; %0.82 MLeft ; 0.67 MRight;  {From empirical findings}
AC.scale_right = 1; 

for k = k_vals
    parfor tau = tau_vals
        % ASSR simulation using Kuramoto model
        [ths] = Kuramoto_simulation(AC,weights,tract_lengths,frequency_mean, sig_f, f_dist,k,tau,t_max,dt,sampling, sig_n);
        ths_new = ths'; rn=sin(ths_new);
        
        % Calculation of spectral power
        f = 39.9:0.01:40.09;
        [Ps,f] = pwelch(rn,[],[],f,100);
        
        RH_pow = log10(Ps(:,~left));
        LH_pow = log10(Ps(:, left));
        
        % Laterality Index
        LI     =  (mean(RH_pow(:)) - mean(LH_pow(:))) /...
              abs((mean(RH_pow(:)) + mean(LH_pow(:))));
          
        LI_Ps(k,tau) = LI; 
    end
waitbar(k/max(k_vals),h,'ASSR (Binaural) simulation...');
end

LI_Ps_Bin=LI_Ps(k_vals,tau_vals);

save([save_path 'ktau_sim_LIs'],'LI_Ps_Bin','k_vals','tau_vals')

clearvars ths* rn Ps 
%  ***************************** Monaural left ********************************
AC.nodes = [29 63];%[Left auditory node  Right auditory node]
%     Scaling power of nodes in case of Binaural condition both would be 1
AC.scale_left = 0.82; %0.82 MLeft ; 0.67 MRight;  {From empirical findings}
AC.scale_right = 1; 

for k = k_vals
    parfor tau = tau_vals
        % ASSR simulation using Kuramoto model
        [ths] = Kuramoto_simulation(AC,weights,tract_lengths,frequency_mean, sig_f, f_dist,k,tau,t_max,dt,sampling, sig_n);
        ths_new = ths'; rn=sin(ths_new);
        
        % Calculation of spectral power
        f = 39.9:0.01:40.09;
        [Ps,f] = pwelch(rn,[],[],f,100);
        
                %foi40  = 3991:4010;
        RH_pow = log10(Ps(:,~left));
        LH_pow = log10(Ps(:, left));
        
        % Laterality Index
        LI     =  (mean(RH_pow(:)) - mean(LH_pow(:))) /...
              abs((mean(RH_pow(:)) + mean(LH_pow(:))));

          
        LI_Ps(k,tau) = LI;                
    end    
waitbar(k/max(k_vals),h,'ASSR (Monaural left) simulation...');
end
LI_Ps_MLeft=LI_Ps(k_vals,tau_vals);
save([save_path 'ktau_sim_LIs'],'LI_Ps_MLeft','-append')

%  ***************************** Monaural right ********************************
AC.nodes = [29 63];%[Left auditory node  Right auditory node]
%     Scaling power of nodes in case of Binaural condition both would be 1
AC.scale_left = 0.67; %0.82 MLeft ; 0.67 MRight;  {From empirical findings}
AC.scale_right = 1; 

for k = k_vals
    parfor tau = tau_vals
        % ASSR simulation using Kuramoto model
        [ths] = Kuramoto_simulation(AC,weights,tract_lengths,frequency_mean, sig_f, f_dist,k,tau,t_max,dt,sampling, sig_n);
        ths_new = ths'; rn=sin(ths_new);
        
        % Calculation of spectral power
        f = 39.9:0.01:40.09;
        [Ps,f] = pwelch(rn,[],[],f,100);
        
                %foi40  = 3991:4010;
        RH_pow = log10(Ps(:,~left));
        LH_pow = log10(Ps(:, left));
        
        % Laterality Index
        LI     =  (mean(RH_pow(:)) - mean(LH_pow(:))) /...
              abs((mean(RH_pow(:)) + mean(LH_pow(:))));

          
        LI_Ps(k,tau) = LI;                
    end    
waitbar(k/max(k_vals),h,'ASSR (Monaural right) simulation...');
end
LI_Ps_MRight=LI_Ps(k_vals,tau_vals);
save([save_path 'ktau_sim_LIs'],'LI_Ps_MRight','-append')

toc
