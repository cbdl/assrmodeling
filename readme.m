% Following document outline the order and details required to run the
% codes posted in the bitbucket <https://bitbucket.org/cbdl/assrmodeling.git> repository.
% 
%
% # Subsequent codes generate the Figures presented in Kumar et al. (2020)
% # Note: Additional files required to run these codes can be obtained at < Google drive link> 
% on request to corresponding author.
% # The source code names are prefixed by 'main'.
% # 
%
% Written by Neeraj Kumar, Nov 2020.
%   Email: neeraj.b15@nbrc.ac.in; neeraj750kumar@gmail.com; 
%%                              Empirical analysis order

%% 1. main_figure1_S1_Group.m  -----------------------------------------(1)
%    Output:
%           spectral power measures of *group* data (Fig. 1 & Supp Fig. S1)
%    # Topoplots, 
%    # Power spectra of ERP, and
%    # Lateralization indices (LI)

%   additional files required
%   # chronux_2_12                                                -toolbox
%   # fieldtrip-20170802                                          -toolbox
%   # GroupEpochs1s(time-series * ch * tr)                        - mat file
%   # colin27_forward_model.mat (consist channel location)        -mat file 
%   # neighbours_sensor (channels nighbours info)                 - mat file
%   # quickcap64 (layout to plot clusters)                         - mat file

%% 2. main_figure1_S1_subjectwise.m  -----------------------------------(2)
%    Output:
%           spectral power measures of *subjectwise* data (Fig. 1 & Supp Fig. S1)
%    # Topoplots, 
%    # Power spectra of ERP, and
%    # Lateralization indices  (LI)

%   additional files required
%   # chronux_2_12          -toolbox
%   # fieldtrip-20170802    -toolbox
%   # topo_layout.mat       -mat file 

%% 3. main_Figure2.m ---------------------------------------------------(3)
%    Output:
%           40 Hz ASSR sources
%           Hemispheric laterality indices at source level
% 
%   additional files required
%   # chronux_2_12                                                -toolbox
%   # fieldtrip-20170802                                          -toolbox
%   # GroupEpochs_CommAvgRef_1s(Average ref time-series* ch * tr) - mat file
%   # colin27_forward_model.mat                                   -mat file 
%   # DK_parcellation                                             - mat file

%%                                Computational Modeling 
%%
% main_ASSR_Sim_ExploreParams.m 
%
% Output:
%       Laterality indices across the parameter space of k and tau (See Figure3 in Kumar et al. 2020)
%
% additional files needed
% # Kuramoto_simulation      -- function                    
% # cortical_network         -- mat file 

% main_ASSR_sim.m
% Output:
%        simulate and plot the LI at specific k and tau 
%
% additional files needed
% # Kuramoto_simulation      -- function                    
% # cortical_network         -- mat file 