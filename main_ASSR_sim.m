%% This code estimate laterality indices by simulating Kuramoto oscillators driven by
% external 40 Hz sin wave input (See Figure3 in Kumar et al. 2020)

% Written by Neeraj Kumar, Nov 2020.

% External files needed
% # Kuramoto_simulation      -- function                    
% # cortical_network          -- mat file 
% From Abeysuriya et al  ('doi: 10.1371/journal.pcbi.1006007')
% ('https://github.com/OHBA-analysis/abeysuriya_wc_isp/tree/master/data_files')


restoredefaultpath
clear;clc;

% From Abeysuriya et al  ('doi: 10.1371/journal.pcbi.1006007')
% ('https://github.com/OHBA-analysis/abeysuriya_wc_isp/tree/master/data_files')

% Please specify paths of folder where the data is downloaded
% supp_path = '/....'; 
% save_path = '/.....';

load([supp_path 'cortical_network.mat'], 'conn','dist','left');

weights = conn;
tract_lengths = dist*1000; % in mm
N=size(weights,1);

% Check that these nodes are defined for 68 total nodes
% Initialize parameters 
t_max=25;
dt=0.0001;
sampling=100; sig_n=0.; 
sig_f=0; frequency_mean =10;
% f_dist =normrnd(frequency_mean,sig_f,[N 1]);
f_dist=zeros(N,1);
k = 30;  
tau = 6;
% LI_Ps = zeros(max(k_vals),max(tau_vals));


%  ***************************** Binaural ********************************
AC.nodes = [29 63];%[Left auditory node  Right auditory node]
%     Scaling power of nodes in case of Binaural condition both would be 1
AC.scale_left  = 1; %0.82 MLeft ; 0.67 MRight;  {From empirical findings}
AC.scale_right = 1; 

% ASSR simulation using Kuramoto model
 [ths] = Kuramoto_simulation(AC,weights,tract_lengths,frequency_mean, sig_f, f_dist,k,tau,t_max,dt,sampling, sig_n);
 ths_new = ths'; rn=sin(ths_new);

% Calculation of spectral power
f = 39.9:0.01:40.09;
[Ps,f] = pwelch(rn,[],[],f,100);

RH_pow = log10(Ps(:,~left));
LH_pow = log10(Ps(:, left));

% Laterality Index
LI_Bin     =  (mean(RH_pow(:)) - mean(LH_pow(:))) /...
    abs((mean(RH_pow(:)) + mean(LH_pow(:))));

sprintf('%.14f',LI_Bin)
clearvars ths* rn Ps 

%  ***************************** Monaural left ********************************
AC.nodes = [29 63];%[Left auditory node  Right auditory node]
%     Scaling power of nodes in case of Binaural condition both would be 1
AC.scale_left = 0.82; %0.82 MLeft ; 0.67 MRight;  {From empirical findings}
AC.scale_right = 1; 

[ths] = Kuramoto_simulation(AC,weights,tract_lengths,frequency_mean, sig_f, f_dist,k,tau,t_max,dt,sampling, sig_n);
ths_new = ths'; rn=sin(ths_new);

% Calculation of spectral power
f = 39.9:0.01:40.09;
[Ps,f] = pwelch(rn,[],[],f,100);

%foi40  = 3991:4010;
RH_pow = log10(Ps(:,~left));
LH_pow = log10(Ps(:, left));

% Laterality Index
LI_MLeft     =  (mean(RH_pow(:)) - mean(LH_pow(:))) /...
    abs((mean(RH_pow(:)) + mean(LH_pow(:))))
clearvars ths* rn Ps 
%  ***************************** Monaural right ********************************
AC.nodes = [29 63];%[Left auditory node  Right auditory node]
%     Scaling power of nodes in case of Binaural condition both would be 1
AC.scale_left = 0.67; %0.82 MLeft ; 0.67 MRight;  {From empirical findings}
AC.scale_right = 1; 

[ths] = Kuramoto_simulation(AC,weights,tract_lengths,frequency_mean, sig_f, f_dist,k,tau,t_max,dt,sampling, sig_n);
ths_new = ths'; rn=sin(ths_new);

% Calculation of spectral power
f = 39.9:0.01:40.09;
[Ps,f] = pwelch(rn,[],[],f,100);

%foi40  = 3991:4010;
RH_pow = log10(Ps(:,~left));
LH_pow = log10(Ps(:, left));

% Laterality Index
LI_MRight     =  (mean(RH_pow(:)) - mean(LH_pow(:))) /...
    abs((mean(RH_pow(:)) + mean(LH_pow(:))))

clearvars ths* rn Ps *_pow
save([save_path 'final_LI'])

%% -----------------------plot LI------
figure;
    
b = bar([LI_MLeft;...
     LI_MRight;...
     LI_Bin]);
    legend({'Theoretical'})
xticklabels({'Monaural left', 'Monaural right', 'Binaural'});ylabel('LI');title('Hemispheric asymmetry')
b.FaceColor = [0.85 .33 .1];

